import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HomeView'),
        centerTitle: true,
      ), 
      body: Center(
        child: Column(
          children: [
            Text(
              'HomeView ', 
              style: TextStyle(fontSize: 20),
            ),
            Text(
              'HomeView ', 
              style: TextStyle(fontSize: 20),
            ),
            Icon(
              Icons.access_time_outlined,
               size: 30,
            )
          ],
        ),
      ),
    );
  }
}
